#!/bin/bash
build() {
    echo "begin build bin...."
    go build -o ./bin/1_create_tab ./1_create_tab/main.go
    go build -o ./bin/2_put_tab ./2_put_tab/main.go
    go build -o ./bin/3-1_batch ./3-1_batch/main.go
    go build -o ./bin/3_batch ./3_batch/main.go
    go build -o ./bin/4-1-iterator ./4-1-iterator/main.go
    go build -o ./bin/4_iterator ./4_iterator/main.go
}

clean() {
    echo "begin clean bin....."
    rm -rf ./bin
    rm -rf data.db
}

printHelp() {
  echo "Usage: "
  echo "  build.sh <mode> "
  echo "    <mode> - one of 'b', 'c'"
  echo "      - 'b' - build bin "
  echo "      - 'c' - clean bin"
}

MODE=$1
if [ "${MODE}" == "b" ]; then
  build
elif [ "${MODE}" == "c" ]; then ## Clear the network
  clean
else
  build
  exit 1
fi

