package main

import (
	"fmt"
	"time"

	"code.uni-ledger.com/switch/kvdb-lib/database/kv"
)

func main() {
	var (
		id      uint64
		jsCount uint64
	)

	dbStore, err := kv.NewKVStore("./data.db")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer dbStore.Close()

	//建立数据表
	err = dbStore.CreateTab([]byte("tab3"), false, []string{"ind1", "ind2"}, nil)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	batch := dbStore.NewBatch([]byte("tab3"))
	defer batch.Rollback()

	var (
		count uint64 = 100000
		i     uint64
	)
	batch.Begin()
	for i = 0; i < count; i++ {
		key := fmt.Sprintf("key%d", i)
		val := fmt.Sprintf("val%d", i)

		tm := time.Now().UTC().Format(time.RFC3339Nano)
		id, err = batch.BatchPut(map[string][][]byte{"ind1": [][]byte{[]byte(key)}, "ind2": [][]byte{[]byte(tm)}},
			nil,
			[]byte([]byte(val)))
		if err != nil {
			fmt.Println(id, err.Error())
			return
		}
	}
	batch.Commit()

	fmt.Println("put data count:", count)
	dbStore.ScanTab([]byte("tab3"), 1, 10000000, true)

	batch.Begin()
	for i = 0; i < count; i++ {
		key := fmt.Sprintf("key%d", i)
		jsCount, err = batch.BatchDelete(true, "ind1", [][]byte{[]byte(key)})
		if err != nil {
			fmt.Println(err.Error())
			return
		}
	}
	batch.Commit()

	fmt.Println("delete data count:", count, jsCount)
	dbStore.ScanTab([]byte("tab3"), 1, 10000000, true)
}
