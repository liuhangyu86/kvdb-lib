package main

import (
	"fmt"
	"sync"
	"time"

	"code.uni-ledger.com/switch/kvdb-lib/database/kv"
)

const (
	gSize = 100000
)

func main() {
	dbStore, err := kv.NewKVStore("./data.db")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer dbStore.Close()

	//建立数据表
	err = dbStore.CreateTab([]byte("tab3-1"), false, []string{"ind1", "ind2"}, nil)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	var (
		n       int32
		goCount int32 = 10
	)

	var gw sync.WaitGroup
	gw.Add(int(goCount))

	/*
		boltdb tx不支持协程安全，因协程操作新建NewBatch操作；
		同时bolt对同一个tab表写入，内部锁保证事务安全；
		因根据实际测试优化代码，慎重设置并发协程数量；
	*/

	Start := time.Now()
	for n = 0; n < goCount; n++ {
		go func(index int32) {
			defer gw.Done()
			var (
				count uint64 = gSize
				i     uint64
				id    uint64
			)

			batch := dbStore.NewBatch([]byte("tab3-1"))
			defer batch.Rollback()
			batch.Begin()

			for i = 0; i < count; i++ {
				key := fmt.Sprintf("%d-key%d", index, i)
				val := fmt.Sprintf("%d-val%d", index, i)
				// fmt.Println(key, val, &key, &val)
				tm := time.Now().UTC().Format(time.RFC3339Nano)
				id, err = batch.BatchPut(map[string][][]byte{"ind1": [][]byte{[]byte(key)}, "ind2": [][]byte{[]byte(tm)}},
					nil,
					[]byte([]byte(val)))
				if err != nil {
					fmt.Println(id, err.Error())
					return
				}
			}
			batch.Commit()
		}(n)
	}
	gw.Wait()

	Latency := time.Now().Sub(Start)
	fmt.Println("write finish:", gSize*goCount, "time:", Latency.String(), "tps:", float64(gSize*goCount)/Latency.Seconds())

	//read
	Start = time.Now()
	size, err := dbStore.ScanTab([]byte("tab3-1"), 1, 1000000000, false)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	Latency = time.Now().Sub(Start)
	fmt.Println("read finish:", len(size), "time:", Latency.String(), "qps:", float64(len(size))/Latency.Seconds())

	// return
	// dbStore.ScanTab([]byte("tab3-1"), 0, 100000000, true)
	// return

	//delete
	gw.Add(int(goCount))
	Start = time.Now()
	for n = 0; n < goCount; n++ {
		go func(index int32) {
			defer gw.Done()
			batch := dbStore.NewBatch([]byte("tab3-1"))
			defer batch.Rollback()
			batch.Begin()

			var (
				count   uint64 = gSize
				i       uint64
				jsCount uint64
			)

			for i = 0; i < count; i++ {
				key := fmt.Sprintf("%d-key%d", index, i)
				jsCount, err = batch.BatchDelete(true, "ind1", [][]byte{[]byte(key)})
				if err != nil {
					fmt.Println(jsCount, err.Error())
					return
				}
			}
			batch.Commit()
		}(n)
	}
	gw.Wait()

	Latency = time.Now().Sub(Start)
	fmt.Println("delete data count:", gSize*goCount, "time:", Latency.String(), "tps:", float64(gSize*goCount)/Latency.Seconds())

	dbStore.ScanTab([]byte("tab3-1"), 1, 100000000, true)
}

/*
100w
无压缩  397M	data.db  value len=18
write finish: 1000000 time: 18.264484258s tps: 54751.06692716995
read finish: 1000000 time: 1.993901074s qps: 501529.39533448487
delete data count: 1000000 time: 6.217603249s tps: 160833.67817990537

压缩  397M	data.db value len=18
write finish: 1000000 time: 18.199661058s tps: 54946.078216134214
read finish: 1000000 time: 1.918322441s qps: 521288.79828915064
delete data count: 1000000 time: 6.285649196s tps: 159092.5565232578


==============
无压缩 743M	data.db  value len=18  len=200
write finish: 1000000 time: 18.50251916s tps: 54046.69447184618
read finish: 1000000 time: 1.874625706s qps: 533439.8204395475
delete data count: 1000000 time: 6.440485956s tps: 155267.78675270508

压缩 540M	data.db
write finish: 1000000 time: 18.755019233s tps: 53319.060224714194
read finish: 1000000 time: 1.970713009s qps: 507430.55707915104
delete data count: 1000000 time: 6.651937934s tps: 150332.13026367963

*/
