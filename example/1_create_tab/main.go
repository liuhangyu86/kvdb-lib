package main

import (
	"fmt"

	"code.uni-ledger.com/switch/kvdb-lib/database/kv"
	"code.uni-ledger.com/switch/kvdb-lib/database/public"
)

func main() {
	dbStore, err := kv.NewKVStore("./data.db")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer dbStore.Close()

	//建立数据表
	err = dbStore.CreateTab([]byte("tab"), false, []string{"uniInd1", "uniInd2"}, []string{"ind1", "ind2"})
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	//100w tab建立
	for i := 0; i < 500; i++ {
		tab := fmt.Sprintf("tab@%d", i)
		err = dbStore.CreateTab([]byte(tab), false, []string{"uniInd1", "uniInd2"}, []string{"ind1", "ind2"})
		if err != nil {
			fmt.Println(err.Error())
			return
		}
	}

	//读取数据表
	fmt.Println("begin read tabs")
	tabs, err := dbStore.GetTabs(true)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	//删除数据表
	fmt.Println("begin drop tabs")
	for tabName := range tabs.(map[string]*public.MetaCache) {
		err := dbStore.DropTab([]byte(tabName))
		if err != nil {
			fmt.Println(err.Error())
			return
		}
	}

	//读取数据表
	fmt.Println("begin read tabs")
	_, err = dbStore.GetTabs(true)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

}
