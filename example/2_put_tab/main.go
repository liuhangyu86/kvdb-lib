package main

import (
	"fmt"
	"time"

	"code.uni-ledger.com/switch/kvdb-lib/database/kv"
)

const (
	size = 3000
)

func main() {
	var (
		id    uint64
		count uint64
	)

	dbStore, err := kv.NewKVStore("./data.db")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer dbStore.Close()

	//建立数据表
	err = dbStore.CreateTab([]byte("tab2"), false, []string{"ind1", "ind2"}, nil)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	for i := 0; i < size; i++ {
		key := fmt.Sprintf("key%d", i)
		val := fmt.Sprintf("val%d", i)
		tm := time.Now().UTC().Format(time.RFC3339Nano)
		id, err = dbStore.Put([]byte("tab2"),
			map[string][][]byte{"ind1": [][]byte{[]byte(key)}, "ind2": [][]byte{[]byte(tm)}},
			nil,
			[]byte([]byte(val)))
		if err != nil {
			fmt.Println(id, err.Error())
			return
		}
	}

	dbStore.ScanTab([]byte("tab2"), 1, size, true)

	for i := 0; i < size; i++ {
		key := fmt.Sprintf("key%d", i)
		val, err := dbStore.Get([]byte("tab2"), "ind1", [][][]byte{[][]byte{[]byte(key)}})
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		flag, err := dbStore.Has([]byte("tab2"), "ind1", [][]byte{[]byte(key)})
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		fmt.Println(key, string(val[0].Value), flag)
	}

	for i := 0; i < size; i++ {
		key := fmt.Sprintf("key%d", i)

		count, err = dbStore.Delete([]byte("tab2"), "ind1", [][]byte{[]byte(key)})
		if err != nil {
			fmt.Println(count, err.Error())
			return
		}

		flag, err := dbStore.Has([]byte("tab2"), "ind1", [][]byte{[]byte(key)})
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		fmt.Println(key, flag)
	}

	dbStore.ScanTab([]byte("tab2"), 1, size, true)

}
