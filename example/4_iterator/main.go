package main

import (
	"bytes"
	"fmt"
	"time"

	"code.uni-ledger.com/switch/kvdb-lib/database/iterator"
	"code.uni-ledger.com/switch/kvdb-lib/database/kv"
)

func main() {
	dbStore, err := kv.NewKVStore("./data.db")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer dbStore.Close()

	//建立数据表
	err = dbStore.CreateTab([]byte("tab4"), false, []string{"ind1", "ind2"}, []string{"ind3"})
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	batch := dbStore.NewBatch([]byte("tab4"))
	defer batch.Rollback()

	var (
		count uint64 = 10
		i     uint64
		id    uint64
	)

	batch.Begin()
	for i = 0; i < count; i++ {
		uniKey1 := fmt.Sprintf("key%d", i)
		uniKey2 := time.Now().UTC().Format(time.RFC3339Nano)
		Key := fmt.Sprintf("key100-key1000-%d", i)
		val := fmt.Sprintf("val%d", i)

		id, err = batch.BatchPut(map[string][][]byte{"ind1": [][]byte{[]byte(uniKey1)}, "ind2": [][]byte{[]byte(uniKey2)}},
			map[string][][]byte{"ind3": [][]byte{[]byte(Key)}},
			[]byte([]byte(val)))
		if err != nil {
			fmt.Println(id, err.Error())
			return
		}
	}
	batch.Commit()

	result, err := dbStore.GetRes([]byte("tab4"), "ind3", [][]byte{[]byte("key100-key")}, 0, 100000)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	if result != nil {
		for i := 0; i < len(result); i++ {
			fmt.Println(string(result[i].Value))
		}
	}

	//迭代器操作
	var (
		iterator iterator.IIterator
		isEnd    = true
	)

	iterator, err = dbStore.NewIterator([]byte("tab4"), "ind3", [][]byte{[]byte("key100")})
	defer iterator.Release()
	for ; err == nil && isEnd; isEnd, err = iterator.Next() {
		fmt.Println("key:", string(bytes.Join(iterator.Key(), []byte(" "))), "   val:", string(iterator.Value()))
	}
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("iterator first")
	if flag, err := iterator.First(); err == nil && flag {
		fmt.Println(string(bytes.Join(iterator.Key(), []byte(" "))), string(iterator.Value()))
	}

	fmt.Println("iterator last")
	if flag, err := iterator.Last(); err == nil && flag {
		fmt.Println(string(bytes.Join(iterator.Key(), []byte(" "))), string(iterator.Value()))
	}

	fmt.Println("iterator seek key100")
	for flag, err := iterator.Seek("ind1", [][]byte{[]byte("key100")}); err == nil && flag; flag, err = iterator.Next() {
		fmt.Println(string(bytes.Join(iterator.Key(), []byte(" "))), string(iterator.Value()))
	}
}
