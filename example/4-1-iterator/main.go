package main

import (
	"bytes"
	"fmt"

	"sync"
	"time"

	"code.uni-ledger.com/switch/kvdb-lib/database/iterator"
	"code.uni-ledger.com/switch/kvdb-lib/database/kv"
)

func main() {
	dbStore, err := kv.NewKVStore("./data.db")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer dbStore.Close()

	//建立数据表
	err = dbStore.CreateTab([]byte("tab4-1"), false, []string{"ind1", "ind2"}, []string{"ind3"})
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	var (
		n       int32
		goCount int32 = 10
	)

	var gw sync.WaitGroup
	gw.Add(int(goCount))

	Start := time.Now()
	for n = 0; n < goCount; n++ {
		go func(index int32) {
			defer gw.Done()
			var (
				count uint64 = 10
				i     uint64
				id    uint64
			)

			batch := dbStore.NewBatch([]byte("tab4-1"))
			defer batch.Rollback()
			batch.Begin()

			for i = 0; i < count; i++ {
				key1 := fmt.Sprintf("%d-1key%d", index, i)
				key2 := fmt.Sprintf("%d-2key%d", index, i)
				key3 := fmt.Sprintf("%d-3key%d", index, i)
				key := fmt.Sprintf("%d-3key", index)

				val := fmt.Sprintf("%d-val%d", index, i)
				// fmt.Println(key, val, &key, &val)
				tm := time.Now().UTC().Format(time.RFC3339Nano)
				id, err = batch.BatchPut(map[string][][]byte{"ind1": [][]byte{[]byte(key1), []byte(key2), []byte(key3)}, "ind2": [][]byte{[]byte(tm)}},
					map[string][][]byte{"ind3": [][]byte{[]byte(key)}},
					[]byte([]byte(val)))
				if err != nil {
					fmt.Println(id, err.Error())
					return
				}
			}
			batch.Commit()
		}(n)
	}
	gw.Wait()

	Latency := time.Now().Sub(Start)
	fmt.Println("write finish:", 100000*goCount, "time:", Latency.String(), "tps:", float64(100000*goCount)/Latency.Seconds())

	var (
		iterator iterator.IIterator
		isEnd    = true
	)

	iterator, err = dbStore.NewIterator([]byte("tab4-1"), "ind3", [][]byte{[]byte("5-3key")})
	defer iterator.Release()
	for ; err == nil && isEnd; isEnd, err = iterator.Next() {
		fmt.Println("key:", string(bytes.Join(iterator.Key(), []byte(" "))), "   val:", string(iterator.Value()))
	}
	if err != nil {
		fmt.Println(err.Error())
		return
	}

}
