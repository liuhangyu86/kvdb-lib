module code.uni-ledger.com/switch/kvdb-lib

go 1.13

require (
	github.com/bndr/gotabulate v1.1.2
	github.com/etcd-io/bbolt v1.3.3
	github.com/golang/protobuf v1.3.2
	github.com/golang/snappy v0.0.1
	github.com/stretchr/testify v1.4.0 // indirect
	go.etcd.io/bbolt v1.3.3 // indirect
	golang.org/x/sys v0.0.0-20191010194322-b09406accb47 // indirect
)
