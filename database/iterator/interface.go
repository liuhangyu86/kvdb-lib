package iterator

type IIterator interface {
	Next() (bool, error)
	Prev() (bool, error)
	First() (bool, error)
	Last() (bool, error)
	Seek(indName string, prefix [][]byte) (bool, error)

	ID() uint64
	Key() [][]byte
	Value() []byte
	Release()
}
