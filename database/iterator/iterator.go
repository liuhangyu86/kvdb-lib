package iterator

import (
	"bytes"

	"code.uni-ledger.com/switch/kvdb-lib/database/codec"
	"code.uni-ledger.com/switch/kvdb-lib/database/model"
	bPrefix "code.uni-ledger.com/switch/kvdb-lib/database/prefix"
	"code.uni-ledger.com/switch/kvdb-lib/database/public"
	bolt "github.com/etcd-io/bbolt"
	"github.com/golang/protobuf/proto"
	"github.com/golang/snappy"
)

//Iterator 迭代器结构定义
type Iterator struct {
	dbIns      *bolt.DB
	cache      map[string]*public.MetaCache
	tab        []byte
	isUniIndex bool
	isCompress bool
	tx         *bolt.Tx

	preKeys []byte
	decKeys []byte

	iter *bolt.Cursor

	iID  uint64
	ikey [][]byte
	ival []byte
}

/*
NewIterator 迭代器 上层可使用迭代器实现前缀匹配、范围查询、过滤、逆序结果集
tab 数据表
isCompress value是否启用压缩
tx 事务执行
preKeys 前缀匹配串
decKeys 存储解析的key值串
iter 游标指针
ikey 获取的索引值
ival 获得的value值
*/
func NewIterator(db *bolt.DB,
	cache map[string]*public.MetaCache,
	tab []byte,
	isUniIndex bool,
	isCompress bool,
	tx *bolt.Tx,
	preKeys []byte,
	decKeys []byte,
	iter *bolt.Cursor,
	iID uint64,
	ikey [][]byte,
	ival []byte) *Iterator {
	return &Iterator{
		dbIns: db,
		cache: cache,
		tab:   tab,

		isUniIndex: isUniIndex,
		isCompress: isCompress,
		tx:         tx,

		preKeys: preKeys,
		decKeys: decKeys,

		iter: iter,
		iID:  iID,
		ikey: ikey,
		ival: ival,
	}
}

/*
Next 迭代器变量

返回:
bool 表示是否可遍历 true可遍历
error 遍历发生的错误
*/
func (it *Iterator) Next() (bool, error) {
	var (
		val   []byte
		value model.Value
		err   error

		tKey []byte
		tVal []byte
	)

	tKey, tVal = it.iter.Next()
	if tKey != nil && !it.isUniIndex {
		deComKey, _ := bPrefix.SplitKeys(tKey)
		_, it.decKeys, err = codec.DecodeBytes(deComKey, it.decKeys)
		if err != nil {
			return false, err
		}

		// fmt.Println("tKey:", tKey, "deComKey:", deComKey, "  decKey:", it.decKeys, " preKeys:", it.preKeys, string(it.decKeys), string(it.preKeys))
		if bytes.HasPrefix(it.decKeys, it.preKeys) {
			val = it.tx.Bucket(bPrefix.TableKey(it.tab)).Get(tVal)
			if val != nil {
				//判断是否解压
				if it.isCompress {
					val, err = snappy.Decode(nil, val)
					if err != nil {
						return false, err
					}
				}

				err = proto.Unmarshal(val, &value)
				if err != nil {
					return false, err
				}

				it.iID = codec.BToU64(value.ID)
				it.ikey = bPrefix.SplitCompositeKey(it.decKeys)
				it.ival = value.Val
				return true, nil
			} else {
				// fmt.Println("val is nil")
				it.iID = 0
				it.ikey = it.ikey[:0]
				it.ival = it.ival[:0]
				return true, nil
			}
		} else {
			// fmt.Println("not find prefix", it.preKeys)
			it.iID = 0
			it.ikey = it.ikey[:0]
			it.ival = it.ival[:0]
			return false, nil
		}
	}

	return false, nil
}

/*
Prev 迭代器变量

返回:
bool 表示是否可遍历 true可遍历
error 遍历发生的错误
*/
func (it *Iterator) Prev() (bool, error) {
	var (
		val   []byte
		value model.Value
		err   error

		tKey []byte
		tVal []byte
	)

	tKey, tVal = it.iter.Prev()
	if tKey != nil && !it.isUniIndex {
		deComKey, _ := bPrefix.SplitKeys(tKey)
		_, it.decKeys, err = codec.DecodeBytes(deComKey, it.decKeys)
		if err != nil {
			return false, err
		}

		// fmt.Println("tKey:", tKey, "deComKey:", deComKey, "  decKey:", it.decKeys, " preKeys:", it.preKeys)
		if bytes.HasPrefix(it.decKeys, it.preKeys) {
			val = it.tx.Bucket(bPrefix.TableKey(it.tab)).Get(tVal)
			if val != nil {
				//判断是否解压
				if it.isCompress {
					val, err = snappy.Decode(nil, val)
					if err != nil {
						return false, err
					}
				}

				err = proto.Unmarshal(val, &value)
				if err != nil {
					return false, err
				}

				it.iID = codec.BToU64(value.ID)
				it.ikey = bPrefix.SplitCompositeKey(it.decKeys)
				it.ival = value.Val
				return true, nil
			} else {
				// fmt.Println("val is nil")
				it.iID = 0
				it.ikey = it.ikey[:0]
				it.ival = it.ival[:0]
				return true, nil
			}
		} else {
			// fmt.Println("not find prefix", it.preKeys)
			it.iID = 0
			it.ikey = it.ikey[:0]
			it.ival = it.ival[:0]
			return false, nil
		}
	}
	return false, nil
}

/*
First 迭代器变量

返回:
bool 表示是否可遍历 true可遍历
error 遍历发生的错误
*/
func (it *Iterator) First() (bool, error) {
	var (
		val   []byte
		value model.Value
		err   error

		tKey []byte
		tVal []byte
	)

	tKey, tVal = it.iter.First()
	if tKey != nil && !it.isUniIndex {
		deComKey, _ := bPrefix.SplitKeys(tKey)
		_, it.decKeys, err = codec.DecodeBytes(deComKey, it.decKeys)
		if err != nil {
			return false, err
		}

		// fmt.Println("tKey:", tKey, "deComKey:", deComKey, "  decKey:", it.decKeys, " preKeys:", it.preKeys)
		if bytes.HasPrefix(it.decKeys, it.preKeys) {
			val = it.tx.Bucket(bPrefix.TableKey(it.tab)).Get(tVal)
			if val != nil {
				//判断是否解压
				if it.isCompress {
					val, err = snappy.Decode(nil, val)
					if err != nil {
						return false, err
					}
				}

				err = proto.Unmarshal(val, &value)
				if err != nil {
					return false, err
				}

				it.iID = codec.BToU64(value.ID)
				it.ikey = bPrefix.SplitCompositeKey(it.decKeys)
				it.ival = value.Val
				return true, nil
			} else {
				// fmt.Println("val is nil")
				it.iID = 0
				it.ikey = it.ikey[:0]
				it.ival = it.ival[:0]
				return true, nil
			}
		} else {
			// fmt.Println("not find prefix", it.preKeys)
			it.iID = 0
			it.ikey = it.ikey[:0]
			it.ival = it.ival[:0]
			return false, nil
		}
	}

	return false, nil
}

/*
Last 迭代器变量

返回:
bool 表示是否可遍历 true可遍历
error 遍历发生的错误
*/
func (it *Iterator) Last() (bool, error) {
	var (
		val   []byte
		value model.Value
		err   error

		tKey []byte
		tVal []byte
	)

	tKey, tVal = it.iter.Last()
	if tKey != nil && !it.isUniIndex {
		deComKey, _ := bPrefix.SplitKeys(tKey)
		_, it.decKeys, err = codec.DecodeBytes(deComKey, it.decKeys)
		if err != nil {
			return false, err
		}

		// fmt.Println("tKey:", tKey, "deComKey:", deComKey, "  decKey:", it.decKeys, " preKeys:", it.preKeys)
		if bytes.HasPrefix(it.decKeys, it.preKeys) {
			val = it.tx.Bucket(bPrefix.TableKey(it.tab)).Get(tVal)
			if val != nil {
				//判断是否解压
				if it.isCompress {
					val, err = snappy.Decode(nil, val)
					if err != nil {
						return false, err
					}
				}

				err = proto.Unmarshal(val, &value)
				if err != nil {
					return false, err
				}

				it.iID = codec.BToU64(value.ID)
				it.ikey = bPrefix.SplitCompositeKey(it.decKeys)
				it.ival = value.Val
				return true, nil
			} else {
				// fmt.Println("val is nil")
				it.iID = 0
				it.ikey = it.ikey[:0]
				it.ival = it.ival[:0]
				return true, nil
			}
		} else {
			// fmt.Println("not find prefix", it.preKeys)
			it.iID = 0
			it.ikey = it.ikey[:0]
			it.ival = it.ival[:0]
			return false, nil
		}
	}
	return false, nil
}

//Seek .
func (it *Iterator) Seek(index string, prefix [][]byte) (bool, error) {
	var (
		val []byte
		err error

		tKey []byte
		tVal []byte

		metaCur    *public.MetaCache
		ok         bool
		isUniIndex bool
	)

	if metaCur, ok = it.cache[string(it.tab)]; !ok {
		return false, public.DetailError([]error{public.ErrTabNotFound}, []string{string(it.tab)})
	}
	//判断索引类型
	if metaCur.UniInds != nil {
		for i := 0; i < len(metaCur.UniInds); i++ {
			if metaCur.UniInds[i] == index {
				it.isUniIndex = true
				break
			}
		}
	}

	if metaCur.IsCompress {
		it.isCompress = true
	}

	getVals := func(it *Iterator, keys []byte, val []byte, meta *public.MetaCache) (uint64, [][]byte, []byte, error) {
		var (
			err error
		)

		if len(val) != 0 {
			if meta.IsCompress {
				val, err = snappy.Decode(nil, val)
				if err != nil {
					return 0, nil, nil, public.DetailError([]error{public.ErrDeCompress, err}, []string{string(it.tab)})
				}
			}

			var value model.Value
			err = proto.Unmarshal(val, &value)
			if err != nil {
				return 0, nil, nil, public.DetailError([]error{public.ErrUnMarshal, err}, []string{string(it.tab)})
			}

			return codec.BToU64(value.ID), bPrefix.SplitCompositeKey(keys), value.Val, nil
		}
		return 0, nil, nil, nil
	}

	if prefix != nil {
		it.preKeys = bPrefix.CreateCompositeKey(prefix)
		tKey, tVal = it.iter.Seek(it.preKeys)
		if tKey != nil {
			if isUniIndex {
				if bytes.HasPrefix(tKey, it.preKeys) {
					val = it.tx.Bucket(bPrefix.TableKey(it.tab)).Get(tVal)
					it.iID, it.ikey, it.ival, err = getVals(it, tKey, val, metaCur)
					if err != nil {
						return false, err
					}
				}
			} else {
				deComKey, _ := bPrefix.SplitKeys(tKey)
				_, it.decKeys, err = codec.DecodeBytes(deComKey, it.decKeys)
				if err != nil {
					return false, err
				}

				if bytes.HasPrefix(it.decKeys, it.preKeys) {
					val = it.tx.Bucket(bPrefix.TableKey(it.tab)).Get(tVal)
					it.iID, it.ikey, it.ival, err = getVals(it, it.decKeys, val, metaCur)
					if err != nil {
						return false, err
					}
				}
			}
		} else {
			return false, nil
		}
	}
	return false, nil
}

func (it *Iterator) ID() uint64 {
	return it.iID
}

//Key .
func (it *Iterator) Key() [][]byte {
	return it.ikey
}

//Value .
func (it *Iterator) Value() []byte {
	return it.ival
}

//Release 释放资源
func (it *Iterator) Release() {
	if it != nil {
		it.tx.Commit()
		it.tab = nil
		it.isUniIndex = false
		it.isCompress = false
		it.tx = nil

		it.preKeys = nil
		it.decKeys = nil

		it.iter = nil
		it.iID = 0
		it.ikey = nil
		it.ival = nil
	}
}
