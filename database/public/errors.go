package public

import (
	"bytes"
	"errors"
	"fmt"
	"runtime"
)

const (
	pad = ','
)

func DetailError(errs []error, msg []string) error {
	var i int
	errsBuf := new(bytes.Buffer)

	if len(errs) != 0 {
		for i = range errs {
			errsBuf.WriteString(errs[i].Error())
			if i != len(errs)-1 {
				errsBuf.WriteByte(byte(pad))
			}
		}
	}

	if len(msg) != 0 {
		errsBuf.WriteByte(byte(pad))
		for i = range msg {
			errsBuf.WriteString(msg[i])
			if i != len(msg)-1 {
				errsBuf.WriteByte(byte(pad))
			}
		}
	}

	_, fn, line, _ := runtime.Caller(1)
	return fmt.Errorf("%s;%s;%d", errsBuf.String(), fn, line)
}

var (
	ErrCreateDB       = errors.New("create database bucket failt")
	ErrCreateTab      = errors.New("create table bucket failt")
	ErrCreateTabIndex = errors.New("create table index bucket failt")

	ErrMarshal   = errors.New("protobuf marshal error")
	ErrUnMarshal = errors.New("protobuf unmarshal error")

	ErrPutKV         = errors.New("put kv error")
	ErrNotFound      = errors.New("not found")
	ErrAlreadyExists = errors.New("already exists")

	ErrTabNotFound = errors.New("table bucket not found")
	ErrIdxNotFound = errors.New("index bucket not found")

	ErrDelTab = errors.New("delete table bucket failt")
	ErrDelInd = errors.New("delete index bucket failt")
	ErrDelKey = errors.New("delete key failt")

	ErrSequence = errors.New("generate next sequence")

	ErrCompress   = errors.New("compress data failt")
	ErrDeCompress = errors.New("decompress data failt")

	ErrEnCodeKey = errors.New("encode keys failt")
	ErrDeCodeKey = errors.New("decode keys failt")

	ErrZeroID   = errors.New("id field must not be a zero value")
	ErrNilParam = errors.New("param must not be nil")

	ErrBeginTransaction = errors.New("begin transaction failt")
)
