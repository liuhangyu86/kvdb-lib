package public

import (
	"bytes"
	"fmt"
	"time"

	"code.uni-ledger.com/switch/kvdb-lib/database/model"
)

//MetaCache 索引缓存对象.
type MetaCache struct {
	UniInds    []string
	Inds       []string
	IsCompress bool
	Tm         time.Time
}

type IndexKeys struct {
	Index string
	Keys  [][]byte
}

func CheckNil(v interface{}, msg ...string) {
	if v == nil {
		panic(fmt.Sprintf("%v %s,param must not be nil", v, msg))
	}
}

// CopyMap map转换
func CopyMap(m map[string][][]byte) map[string]*model.KeyList {
	if m != nil {
		newMap := make(map[string]*model.KeyList)
		for k, v := range m {
			newMap[k] = &model.KeyList{Indkeys: v}
		}
		return newMap
	}

	return nil
}

func CopyKeys(param [][]byte) [][]byte {
	var retResult [][]byte
	if len(param) != 0 {
		retResult = make([][]byte, len(param))
		copy(retResult, param)
	}
	return retResult
}

func IsBefore(key, max []byte) bool {
	return key != nil && bytes.Compare(key, max) <= 0
}

type Item struct {
	ID    uint64
	Keys  [][]byte
	Value []byte
}
