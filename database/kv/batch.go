package kv

import (
	"bytes"
	"fmt"

	"code.uni-ledger.com/switch/kvdb-lib/database/codec"
	"code.uni-ledger.com/switch/kvdb-lib/database/model"
	bPrefix "code.uni-ledger.com/switch/kvdb-lib/database/prefix"
	"code.uni-ledger.com/switch/kvdb-lib/database/public"
	bolt "github.com/etcd-io/bbolt"
	"github.com/golang/protobuf/proto"
	"github.com/golang/snappy"
)

// Batch 批量操作put、delete对象
type Batch struct {
	dbIns      *bolt.DB
	tab        []byte
	isCompress bool
	tx         *bolt.Tx
}

//NewBatch .
func (kv *KVStore) NewBatch(tab []byte) *Batch {
	var (
		tabName []byte
		metaCur *public.MetaCache
		ok      bool
	)

	if metaCur, ok = kv.cache[string(tab)]; !ok {
		public.CheckNil(tab, "NewBatch", "table not exist")
	}

	tabName = make([]byte, len(tab))
	copy(tabName, tab)

	return &Batch{
		dbIns:      kv.dbIns,
		tab:        tabName,
		isCompress: metaCur.IsCompress,
		tx:         nil,
	}
}

// Begin tx begin.
func (b *Batch) Begin() error {
	var err error
	b.tx, err = b.dbIns.Begin(true)
	if err != nil {
		return public.DetailError([]error{public.ErrBeginTransaction, err}, []string{string(b.tab)})
	}
	return nil
}

//Commit .
func (b *Batch) Commit() error {
	var err error
	if b != nil && b.tx != nil {
		err = b.tx.Commit()
		b.tab = nil
		b.isCompress = false
		b.tx = nil
		return err
	}
	return err
}

//Rollback .
func (b *Batch) Rollback() error {
	var err error
	if b != nil && b.tx != nil {
		err = b.tx.Rollback()
		b.tab = nil
		b.isCompress = false
		b.tx = nil
	}
	return err
}

/*
BatchPut 批量put
unikey 建立唯一索引的key值切片
key 建立非唯一索引的key值切片
val 存储的val数值

返回
uint64 主表数据id值
err 失败返回错误信息
*/
func (b *Batch) BatchPut(uniInds map[string][][]byte, inds map[string][][]byte, val []byte) (uint64, error) {
	var (
		index []byte
		err   error
	)

	//1 获取主表id以及创建主表kv数据
	bkt := b.tx.Bucket(bPrefix.TableKey(b.tab))
	id, err := bkt.NextSequence()
	if err != nil {
		return 0, public.DetailError([]error{public.ErrSequence, err}, []string{string(b.tab)})
	}

	index = codec.U64ToB(id)
	valBytes, err := proto.Marshal(&model.Value{
		UniInds: public.CopyMap(uniInds),
		Inds:    public.CopyMap(inds),
		ID:      index,
		Val:     val,
	})
	if err != nil {
		return 0, public.DetailError([]error{public.ErrMarshal, err}, []string{string(b.tab)})
	}

	if b.isCompress {
		valBytes = snappy.Encode(nil, valBytes)
	}

	err = bkt.Put(index, valBytes)
	if err != nil {
		return 0, public.DetailError([]error{public.ErrPutKV, err}, []string{string(b.tab)})
	}

	//2 建立唯一索引数据映射
	if uniInds != nil {
		for indName, indKeys := range uniInds {
			bkt = b.tx.Bucket(bPrefix.IndexKey(b.tab, []byte(indName)))
			if bkt == nil {
				return 0, public.DetailError([]error{public.ErrIdxNotFound, err}, []string{string(b.tab), indName})
			}

			err = bkt.Put(bPrefix.CreateCompositeKey(indKeys), index)
			if err != nil {
				return 0, public.DetailError([]error{public.ErrPutKV, err}, []string{string(b.tab)})
			}
		}
	}

	//3 建立非唯一索引数据映射
	if inds != nil {
		for indName, indKeys := range inds {
			bkt = b.tx.Bucket(bPrefix.IndexKey(b.tab, []byte(indName)))
			if bkt == nil {
				return 0, public.DetailError([]error{public.ErrIdxNotFound, err}, []string{string(b.tab), indName})
			}

			err = bkt.Put(bPrefix.JoinKeys(codec.EncodeBytes(nil, bPrefix.CreateCompositeKey(indKeys)), index), index)
			if err != nil {
				return 0, public.DetailError([]error{public.ErrPutKV, err}, []string{string(b.tab)})
			}
		}
	}

	return id, nil
}

/*
BatchDelete 批量删除
idxType true为唯一索引,否则为非唯一索引
index  索引名(可唯一或者非唯一索引)
key  建立索引的keys值切片

返回
uint64 删除的记录大小
err 失败返回错误信息
*/
func (b *Batch) BatchDelete(idxType bool, index string, keys [][]byte) (uint64, error) {
	var (
		err   error
		count uint64
	)

	doDel := func(tab []byte, id []byte, val []byte, isCompress bool, tx *bolt.Tx) error {
		var (
			err error
		)

		if len(val) != 0 {
			//2 判断是否解压
			if isCompress {
				val, err = snappy.Decode(nil, val)
				if err != nil {
					return public.DetailError([]error{public.ErrDeCompress, err}, []string{string(tab)})
				}
			}

			var value model.Value
			err = proto.Unmarshal(val, &value)
			if err != nil {
				return public.DetailError([]error{public.ErrUnMarshal, err}, []string{string(tab)})
			}

			//4 删除唯一索引数据
			if value.UniInds != nil {
				for indexName, uniKeys := range value.UniInds {
					bkt := tx.Bucket(bPrefix.IndexKey(tab, []byte(indexName)))
					if bkt != nil {
						err = bkt.Delete(bPrefix.CreateCompositeKey(uniKeys.Indkeys))
						if err != nil {
							return public.DetailError([]error{public.ErrDelKey, err}, []string{string(tab)})
						}
					}
				}
			}

			//5 删除非唯一索引数据
			if value.Inds != nil {
				for indexName, keys := range value.Inds {
					bkt := tx.Bucket(bPrefix.IndexKey(tab, []byte(indexName)))
					if bkt != nil {
						err = bkt.Delete(bPrefix.JoinKeys(codec.EncodeBytes(nil, bPrefix.CreateCompositeKey(keys.Indkeys)), id))
						if err != nil {
							return public.DetailError([]error{public.ErrDelKey, err}, []string{string(tab)})
						}
					}
				}
			}
		}
		//3 删除表数据
		err = tx.Bucket(bPrefix.TableKey(tab)).Delete(id)
		if err != nil {
			return public.DetailError([]error{public.ErrDelKey, err}, []string{string(tab)})
		}

		return nil
	}

	if idxType {
		bkt := b.tx.Bucket(bPrefix.IndexKey(b.tab, []byte(index)))
		if bkt != nil {
			//1 获取id索引
			id := bkt.Get(bPrefix.CreateCompositeKey(keys))
			if id != nil {
				//2 根据id索引获取值
				val := b.tx.Bucket(bPrefix.TableKey(b.tab)).Get(id)
				err = doDel(b.tab, id, val, b.isCompress, b.tx)
				if err != nil {
					return 0, err
				}
				count++
			} else {
				fmt.Printf("%s not exist used for index\n", bytes.Join(keys, nil))
			}
		}
	} else {
		bkt := b.tx.Bucket(bPrefix.IndexKey(b.tab, []byte(index)))
		if bkt != nil {
			cur := bkt.Cursor()
			comKey := bPrefix.CreateCompositeKey(keys)
			for k, id := cur.Seek(comKey); k != nil; k, id = cur.Next() {
				//模糊查找
				if bytes.HasPrefix(k, comKey) {
					val := b.tx.Bucket(bPrefix.TableKey(b.tab)).Get(id)
					err = doDel(b.tab, id, val, b.isCompress, b.tx)
					if err != nil {
						return 0, err
					}
					count++
				}
			}
		}
	}
	return count, nil
}
