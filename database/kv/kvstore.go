package kv

import (
	"bytes"
	"fmt"
	"strings"
	"time"

	"code.uni-ledger.com/switch/kvdb-lib/database/codec"
	"code.uni-ledger.com/switch/kvdb-lib/database/iterator"
	"code.uni-ledger.com/switch/kvdb-lib/database/model"
	bPrefix "code.uni-ledger.com/switch/kvdb-lib/database/prefix"
	"code.uni-ledger.com/switch/kvdb-lib/database/public"
	"github.com/bndr/gotabulate"
	bolt "github.com/etcd-io/bbolt"
	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/snappy"
)

//KVStore 存储对象 .
type KVStore struct {
	dbIns *bolt.DB
	cache map[string]*public.MetaCache
}

//NewKVStore 实例化存储对象.
func NewKVStore(file string) (*KVStore, error) {
	o := bolt.Options{
		Timeout: 3 * time.Second,
	}

	ins, err := bolt.Open(file, 0600, &o)
	if err != nil {
		return nil, err
	}

	kvStore := &KVStore{
		dbIns: ins,
		cache: make(map[string]*public.MetaCache),
	}

	//加载表索引数据
	metaArray, err := kvStore.getMetaInfo()
	if err != nil {
		return nil, err
	}
	if metaArray != nil {
		for i := 0; i < len(metaArray); i++ {
			kvStore.cache[string(metaArray[i].Tab)] = &public.MetaCache{
				UniInds:    metaArray[i].UniInds,
				Inds:       metaArray[i].Inds,
				IsCompress: metaArray[i].IsCompress,
			}
		}
	}

	return kvStore, nil
}

//Close 销毁实例化对象
func (kv *KVStore) Close() error {
	if kv != nil && kv.dbIns != nil {
		err := kv.dbIns.Close()
		//https://github.com/boltdb/bolt/issues/397
		if err == nil {
			kv.cache = nil
			kv.dbIns = nil
		}
		return err
	}
	return nil
}

//getMetaInfo 获取数据库所有表元数据
func (kv *KVStore) getMetaInfo() ([]*model.MetaData, error) {
	var (
		err      error
		metaInfo []*model.MetaData
	)

	err = kv.dbIns.View(func(tx *bolt.Tx) error {
		bkt := tx.Bucket(bPrefix.DBKey(bPrefix.DBNAME))
		if bkt != nil {
			bkt.ForEach(func(k, v []byte) error {
				metaData := &model.MetaData{}
				err := proto.Unmarshal(v, metaData)
				if err != nil {
					return err
				}

				metaInfo = append(metaInfo, metaData)
				return nil
			})
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	return metaInfo, nil
}

/*
GetTabs 获取数据库所有表
isPrint 是否打印元数据
*/
func (kv *KVStore) GetTabs(isPrint bool) (interface{}, error) {
	var (
		err      error
		tm       time.Time
		tabMeta  map[string]*public.MetaCache
		metaInfo [][]string
	)

	metaArray, err := kv.getMetaInfo()
	if err != nil {
		return nil, err
	}

	tabMeta = make(map[string]*public.MetaCache)
	if metaArray == nil {
		return tabMeta, nil
	}

	for i := 0; i < len(metaArray); i++ {
		tm, err = ptypes.Timestamp(metaArray[i].Tm)
		if err != nil {
			return nil, err
		}

		tabMeta[string(metaArray[i].Tab)] = &public.MetaCache{
			UniInds:    metaArray[i].UniInds,
			Inds:       metaArray[i].Inds,
			IsCompress: metaArray[i].IsCompress,
			Tm:         tm,
		}

		if isPrint {
			metaInfo = append(metaInfo, []string{string(metaArray[i].Tab),
				strings.Join(metaArray[i].UniInds, " "),
				strings.Join(metaArray[i].Inds, " "),
				tm.String()})
		}
	}

	//打印数据表
	if isPrint && len(metaInfo) != 0 {
		tabulate := gotabulate.Create(metaInfo)
		tabulate.SetHeaders([]string{"table", "unique index", "index", "create time"})
		tabulate.SetMaxCellSize(150)
		tabulate.SetWrapStrings(true)
		tabulate.SetEmptyString("None")
		tabulate.SetAlign("left")
		fmt.Println(tabulate.Render("grid"))
		metaInfo = nil
	}

	return tabMeta, err
}

/*
CreateTab 创建表
tab 表明
isCompress 是否启用压缩(建议都启用压缩)
uniInds 唯一索引名
inds 非唯一索引名
*/
func (kv *KVStore) CreateTab(tab []byte, isCompress bool, uniInds []string, inds []string) error {
	return kv.dbIns.Update(func(tx *bolt.Tx) error {
		//1 建表
		_, err := tx.CreateBucketIfNotExists(bPrefix.TableKey(tab))
		if err != nil {
			return public.DetailError([]error{public.ErrCreateTab, err}, []string{string(tab)})
		}

		//2 索引空间建立
		{
			if uniInds != nil {
				//建立唯一索引
				for i := 0; i < len(uniInds); i++ {
					_, err = tx.CreateBucketIfNotExists(bPrefix.IndexKey(tab, []byte(uniInds[i])))
					if err != nil {
						return public.DetailError([]error{public.ErrCreateTabIndex, err}, []string{string(tab)})
					}
				}
			}

			if inds != nil {
				//建立非唯一索引
				for i := 0; i < len(inds); i++ {
					_, err = tx.CreateBucketIfNotExists(bPrefix.IndexKey(tab, []byte(inds[i])))
					if err != nil {
						return public.DetailError([]error{public.ErrCreateTabIndex, err}, []string{string(tab)})
					}
				}
			}
		}

		//3 保存元数据
		bkt, err := tx.CreateBucketIfNotExists(bPrefix.DBKey(bPrefix.DBNAME))
		if err != nil {
			return public.DetailError([]error{public.ErrCreateDB, err}, []string{bPrefix.DBNAME})
		}

		//4 不存在则创建元数据
		metaData := bkt.Get(bPrefix.TableKey(tab))
		if metaData == nil {
			//存储表元数据
			valBytes, err := proto.Marshal(&model.MetaData{
				Tab:     tab,
				UniInds: uniInds,
				Inds:    inds,
				Tm:      ptypes.TimestampNow(),
				//https://s0godoc0org.icopy.site/github.com/golang/protobuf/ptypes#TimestampNow
				IsCompress: isCompress,
			})
			if err != nil {
				return public.DetailError([]error{public.ErrMarshal, err}, []string{string(tab)})
			}

			err = bkt.Put(bPrefix.TableKey(tab), valBytes)
			if err != nil {
				return public.DetailError([]error{public.ErrPutKV, err}, []string{string(tab)})
			}

			kv.cache[string(tab)] = &public.MetaCache{
				UniInds:    uniInds,
				Inds:       inds,
				IsCompress: isCompress,
			}
		}
		return nil
	})
}

/*
DropTab 删除表
tab 表名
*/
func (kv *KVStore) DropTab(tab []byte) error {
	var (
		err     error
		ok      bool
		tabMeta *public.MetaCache
	)

	if tabMeta, ok = kv.cache[string(tab)]; !ok {
		return public.DetailError([]error{public.ErrTabNotFound}, []string{string(tab)})
	}

	return kv.dbIns.Update(func(tx *bolt.Tx) error {
		//1 删除表数据
		bkt := tx.Bucket(bPrefix.TableKey(tab))
		if bkt != nil {
			bkt.ForEach(func(k, v []byte) error {
				err = bkt.Delete(k)
				if err != nil {
					return public.DetailError([]error{public.ErrDelKey, err}, []string{string(tab)})
				}
				return nil
			})
		}

		//2 删除表桶
		err := tx.DeleteBucket(bPrefix.TableKey(tab))
		if err != nil {
			if err != nil {
				return public.DetailError([]error{public.ErrDelTab, err}, []string{string(tab)})
			}
			return nil
		}

		//3 删除索引
		{
			//删除唯一索引数据
			if len(tabMeta.UniInds) != 0 {
				for i := 0; i < len(tabMeta.UniInds); i++ {
					bkt := tx.Bucket(bPrefix.IndexKey(tab, []byte(tabMeta.UniInds[i])))
					if bkt != nil {
						bkt.ForEach(func(k, v []byte) error {
							err = bkt.Delete(k)
							if err != nil {
								return public.DetailError([]error{public.ErrDelKey, err}, []string{string(tab)})
							}
							return nil
						})
					}
				}
			}

			//删除非唯一索引数据
			if len(tabMeta.Inds) != 0 {
				for i := 0; i < len(tabMeta.Inds); i++ {
					bkt := tx.Bucket(bPrefix.IndexKey(tab, []byte(tabMeta.Inds[i])))
					if bkt != nil {
						bkt.ForEach(func(k, v []byte) error {
							err = bkt.Delete(k)
							if err != nil {
								return public.DetailError([]error{public.ErrDelKey, err}, []string{string(tab)})
							}
							return nil
						})
					}
				}
			}

			//4 删除库元数据
			return tx.Bucket(bPrefix.DBKey(bPrefix.DBNAME)).Delete(bPrefix.TableKey(tab))
		}
	})
}

/*
Put 保存KV数据
tab 表名
uniInds 唯一索引map,k为索引名,v为建立索引的key值切片
inds 唯一索引map,k为索引名,v为建立索引的key值切片
val 存储的val数值

返回
uint64 主表数据id值
err 失败返回错误信息
**/
func (kv *KVStore) Put(tab []byte, uniInds map[string][][]byte, inds map[string][][]byte, val []byte) (uint64, error) {
	var (
		id      uint64
		index   []byte
		err     error
		tabMeta *public.MetaCache
		ok      bool
	)

	if tabMeta, ok = kv.cache[string(tab)]; !ok {
		return 0, public.DetailError([]error{public.ErrTabNotFound}, []string{string(tab)})
	}

	err = kv.dbIns.Update(func(tx *bolt.Tx) error {
		//1 获取主表id,存储主表数据
		bkt := tx.Bucket(bPrefix.TableKey(tab))
		id, err = bkt.NextSequence()
		if err != nil {
			return public.DetailError([]error{public.ErrSequence, err}, []string{string(tab)})
		}

		index = codec.U64ToB(id)
		valBytes, err := proto.Marshal(&model.Value{
			UniInds: public.CopyMap(uniInds),
			Inds:    public.CopyMap(inds),
			ID:      index,
			Val:     val,
		})
		if err != nil {
			return public.DetailError([]error{public.ErrMarshal, err}, []string{string(tab)})
		}

		if tabMeta.IsCompress {
			valBytes = snappy.Encode(nil, valBytes)
		}

		err = bkt.Put(index, valBytes)
		if err != nil {
			return public.DetailError([]error{public.ErrPutKV, err}, []string{string(tab)})
		}

		//2 建立唯一索引数据映射
		if uniInds != nil {
			for indName, indKeys := range uniInds {
				bkt := tx.Bucket(bPrefix.IndexKey(tab, []byte(indName)))
				if bkt == nil {
					return public.DetailError([]error{public.ErrIdxNotFound}, []string{string(tab), indName})
				}

				err = bkt.Put(bPrefix.CreateCompositeKey(indKeys), index)
				if err != nil {
					return public.DetailError([]error{public.ErrPutKV, err}, []string{string(tab)})
				}
			}
		}

		//3 建立非唯一索引数据映射
		if inds != nil {
			for indName, indKeys := range inds {
				bkt := tx.Bucket(bPrefix.IndexKey(tab, []byte(indName)))
				if bkt == nil {
					return public.DetailError([]error{public.ErrIdxNotFound}, []string{string(tab), indName})
				}

				err = bkt.Put(bPrefix.JoinKeys(codec.EncodeBytes(nil, bPrefix.CreateCompositeKey(indKeys)), index), index)
				if err != nil {
					return public.DetailError([]error{public.ErrPutKV, err}, []string{string(tab)})
				}
			}
		}

		return nil
	})

	return id, err
}

/*
Update 更新KV数据
tab 表名
cond 根据唯一索引条件更新
uniInds  更新的唯一索引值
inds  更新的非唯一索引值
val 存储的val数值

返回
uint64 主表数据id值
err 失败返回错误信息
**/
func (kv *KVStore) Update(tab []byte, cond *public.IndexKeys,
	uniInds []*public.IndexKeys, inds []*public.IndexKeys, val []byte) (uint64, error) {
	var (
		err     error
		id      uint64
		tabMeta *public.MetaCache
		ok      bool
	)

	if tabMeta, ok = kv.cache[string(tab)]; !ok {
		return 0, public.DetailError([]error{public.ErrTabNotFound}, []string{string(tab)})
	}

	doUpdate := func(tab []byte, id []byte, oldVal []byte, uniInds []*public.IndexKeys, inds []*public.IndexKeys, newVal []byte,
		meta *public.MetaCache, tx *bolt.Tx) error {
		var (
			err error
			val []byte
		)

		//2 判断是否解压
		if meta.IsCompress {
			val, err = snappy.Decode(nil, oldVal)
			if err != nil {
				return public.DetailError([]error{public.ErrDeCompress, err}, []string{string(tab)})
			}
		}

		var value model.Value
		err = proto.Unmarshal(val, &value)
		if err != nil {
			return public.DetailError([]error{public.ErrUnMarshal, err}, []string{string(tab)})
		}

		//更新唯一索引数据
		if value.UniInds != nil && len(uniInds) != 0 {
			for i := range uniInds {
				if keyList, ok := value.UniInds[uniInds[i].Index]; ok {
					bkt := tx.Bucket(bPrefix.IndexKey(tab, []byte(uniInds[i].Index)))
					if bkt != nil {
						//删除旧索引
						err = bkt.Delete(bPrefix.CreateCompositeKey(keyList.Indkeys))
						if err != nil {
							return public.DetailError([]error{public.ErrDelKey, err}, []string{string(tab)})
						}

						//建立新索引
						err = bkt.Put(bPrefix.CreateCompositeKey(uniInds[i].Keys), id)
						if err != nil {
							return public.DetailError([]error{public.ErrPutKV, err}, []string{string(tab)})
						}
					}
				} else {
					return public.DetailError([]error{public.ErrIdxNotFound}, []string{string(tab)})
				}
			}
		}

		//更新非唯一索引数据
		if value.Inds != nil && len(inds) != 0 {
			for i := range inds {
				if keyList, ok := value.UniInds[inds[i].Index]; ok {
					bkt := tx.Bucket(bPrefix.IndexKey(tab, []byte(inds[i].Index)))
					if bkt != nil {
						//删除旧索引
						err = bkt.Delete(bPrefix.JoinKeys(codec.EncodeBytes(nil, bPrefix.CreateCompositeKey(keyList.Indkeys)), id))
						if err != nil {
							return public.DetailError([]error{public.ErrDelKey, err}, []string{string(tab)})
						}

						//建立新索引
						err = bkt.Put(bPrefix.JoinKeys(codec.EncodeBytes(nil, bPrefix.CreateCompositeKey(inds[i].Keys)), id), id)
						if err != nil {
							return public.DetailError([]error{public.ErrPutKV, err}, []string{string(tab)})
						}
					}
				} else {
					return public.DetailError([]error{public.ErrIdxNotFound}, []string{string(tab)})
				}
			}
		}
		return nil
	}

	err = kv.dbIns.Update(func(tx *bolt.Tx) error {
		bkt := tx.Bucket(bPrefix.IndexKey(tab, []byte(cond.Index)))
		if bkt != nil {
			idBytes := bkt.Get(bPrefix.CreateCompositeKey(cond.Keys))
			if idBytes != nil {
				oldVal := tx.Bucket(bPrefix.TableKey(tab)).Get(idBytes)
				if len(val) != 0 {
					err = doUpdate(tab, idBytes, oldVal, uniInds, inds, val, tabMeta, tx)
					if err != nil {
						return err
					}
				}

				id = codec.BToU64(idBytes)
			}
		} else {
			return public.DetailError([]error{public.ErrIdxNotFound, err}, []string{string(tab)})
		}
		return nil
	})

	return id, err
}

/*
ScanTab 获取表所有数据
tab 表明
skip 翻页开始下标，从1开始
limit 每页大小
isPrint true终端输出数据

返回
Item切片
err 失败返回错误信息
*/
func (kv *KVStore) ScanTab(tab []byte, skip uint64, limit uint64, isPrint bool) ([]*public.Item, error) {
	var (
		err     error
		tabMeta *public.MetaCache
		ok      bool
		count   uint64
	)
	tabItems := make([]*public.Item, 0, limit)

	if tabMeta, ok = kv.cache[string(tab)]; !ok {
		return nil, public.DetailError([]error{public.ErrTabNotFound}, []string{string(tab)})
	}

	var (
		pCount  uint64
		tabHead []string
		tabRows [][]string
	)

	//打印表头
	if isPrint {
		tabHead = append(tabHead, "count")
		tabHead = append(tabHead, "ID")
		if len(tabMeta.UniInds) != 0 {
			tabHead = append(tabHead, tabMeta.UniInds...)
		}

		if len(tabMeta.Inds) != 0 {
			tabHead = append(tabHead, tabMeta.Inds...)
		}
		tabHead = append(tabHead, "value")
	}

	err = kv.dbIns.View(func(tx *bolt.Tx) error {
		bkt := tx.Bucket(bPrefix.TableKey(tab))
		if bkt != nil {
			var (
				id uint64
				k  []byte
				v  []byte
			)

			cur := bkt.Cursor()
			for k, v = cur.Seek([]byte(codec.U64ToB(skip))); k != nil && count < limit; k, v = cur.Next() {
				count++

				//判断是否解压
				if tabMeta.IsCompress {
					v, err = snappy.Decode(nil, v)
					if err != nil {
						return public.DetailError([]error{public.ErrDeCompress, err}, []string{string(tab)})
					}
				}

				var value model.Value
				err = proto.Unmarshal(v, &value)
				if err != nil {
					return public.DetailError([]error{public.ErrUnMarshal, err}, []string{string(tab)})
				}
				id = codec.BToU64(k)

				if isPrint {
					pCount++
					var row []string
					row = append(row, fmt.Sprintf("%d", count))
					row = append(row, fmt.Sprintf("%d", id))

					if value.UniInds != nil {
						for i := 0; i < len(tabMeta.UniInds); i++ { //有序遍历
							if uniKeyList, ok := value.UniInds[tabMeta.UniInds[i]]; ok && len(uniKeyList.Indkeys) != 0 {
								row = append(row, string(bytes.Join(uniKeyList.Indkeys, []byte(" "))))
							} else {
								row = append(row, string(" "))
							}
						}
					}

					if value.Inds != nil {
						for i := 0; i < len(tabMeta.Inds); i++ { //有序遍历
							if keyList, ok := value.Inds[tabMeta.Inds[i]]; ok && len(keyList.Indkeys) != 0 {
								row = append(row, string(bytes.Join(keyList.Indkeys, []byte(" "))))
							} else {
								row = append(row, string(" "))
							}
						}
					}

					row = append(row, string(value.Val))
					tabRows = append(tabRows, row)

					if pCount >= 1500 && len(tabRows) != 0 {
						tabulate := gotabulate.Create(tabRows)
						tabulate.SetHeaders(tabHead)
						tabulate.SetMaxCellSize(1500)
						tabulate.SetWrapStrings(true)
						tabulate.SetEmptyString("None")
						tabulate.SetAlign("left")
						fmt.Println(tabulate.Render("grid"))
						tabRows = tabRows[:0]
						pCount = 0
					}
				}

				if v != nil && value.Val != nil {
					tabItems = append(tabItems, &public.Item{
						ID:    id,
						Keys:  nil,
						Value: value.Val,
					})
				}
			}
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	if len(tabRows) != 0 {
		tabulate := gotabulate.Create(tabRows)
		tabulate.SetHeaders(tabHead)
		tabulate.SetMaxCellSize(1500)
		// tabulate.SetWrapStrings(true)
		tabulate.SetEmptyString("None")
		tabulate.SetAlign("left")
		fmt.Println(tabulate.Render("grid"))
	}

	return tabItems, nil
}

/*
Get 根据唯一索引获取value
tab 表名
uniInd 唯一索引名
unikeys 多个唯一索引的key值切片

返回
Item指针
err 失败返回错误信息
**/
func (kv *KVStore) Get(tab []byte, uniInd string, unikeys [][][]byte) ([]*public.Item, error) {
	var (
		err   error
		val   []byte
		value model.Value
	)
	itemRes := make([]*public.Item, 0, len(unikeys))

	err = kv.dbIns.View(func(tx *bolt.Tx) error {
		bkt := tx.Bucket(bPrefix.IndexKey(tab, []byte(uniInd)))
		if bkt != nil {
			for i := 0; i < len(unikeys); i++ {
				id := bkt.Get(bPrefix.CreateCompositeKey(unikeys[i]))
				if id != nil {
					val = tx.Bucket(bPrefix.TableKey(tab)).Get(id)

					//判断是否解压
					if kv.cache[string(tab)].IsCompress {
						val, err = snappy.Decode(nil, val)
						if err != nil {
							return public.DetailError([]error{public.ErrDeCompress, err}, []string{string(tab)})
						}
					}

					err = proto.Unmarshal(val, &value)
					if err != nil {
						return public.DetailError([]error{public.ErrUnMarshal, err}, []string{string(tab)})
					}
					val = value.Val

					var elem public.Item
					elem.ID = codec.BToU64(id)
					elem.Keys = public.CopyKeys(unikeys[i])
					elem.Value = val
					itemRes = append(itemRes, &elem)
				}
			}

		} else {
			return public.DetailError([]error{public.ErrIdxNotFound, err}, []string{string(tab)})
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	return itemRes, nil
}

/*
GetRes 根据索引获取value集合
tab 表名
index  索引名(可唯一或者非唯一索引)
key  建立索引的keys值切片
skip 翻页开始下标，从1开始
limit 每页大小

返回
Item指针
err 失败返回错误信息
**/
func (kv *KVStore) GetRes(tab []byte, index string, keys [][]byte, skip uint64, limit uint64) ([]*public.Item, error) {
	var (
		err      error
		val      []byte
		deComKey []byte

		metaCur    *public.MetaCache
		ok         bool
		isUniIndex bool

		count uint64
	)

	if metaCur, ok = kv.cache[string(tab)]; !ok {
		return nil, public.DetailError([]error{public.ErrTabNotFound}, []string{string(tab)})
	}

	//判断索引类型
	if metaCur.UniInds != nil {
		for i := 0; i < len(metaCur.UniInds); i++ {
			if metaCur.UniInds[i] == index {
				isUniIndex = true
				break
			}
		}
	}

	iKey := make([]byte, 60)
	tabItems := make([]*public.Item, 0)

	doParser := func(id []byte, k []byte, val []byte, meta *public.MetaCache, tabItems []*public.Item) error {
		if len(val) != 0 {
			if meta.IsCompress {
				val, err = snappy.Decode(nil, val)
				if err != nil {
					return public.DetailError([]error{public.ErrDeCompress, err}, []string{string(tab)})
				}
			}

			var value model.Value
			err = proto.Unmarshal(val, &value)
			if err != nil {
				return public.DetailError([]error{public.ErrUnMarshal, err}, []string{string(tab)})
			}

			tabItems = append(tabItems, &public.Item{
				ID:    codec.BToU64(id),
				Keys:  bPrefix.SplitCompositeKey(k),
				Value: value.Val,
			})
		}
		return nil
	}

	err = kv.dbIns.View(func(tx *bolt.Tx) error {
		bkt := tx.Bucket(bPrefix.IndexKey(tab, []byte(index)))
		if bkt != nil {
			var (
				k  []byte
				id []byte
			)

			cur := bkt.Cursor()
			compKey := bPrefix.CreateCompositeKey(keys)

			if isUniIndex {
				for k, id = cur.Seek(compKey); k != nil && count < limit; k, id = cur.Next() {
					if bytes.HasPrefix(k, compKey) {
						count++
						if skip > 1 {
							if count == skip {
								count = 1
							} else {
								continue
							}
						}

						val = tx.Bucket(bPrefix.TableKey(tab)).Get(id)
						err = doParser(id, k, val, metaCur, tabItems)
						if err != nil {
							return err
						}
					}
				}
			} else {
				for k, id = cur.Seek(compKey); k != nil && count < limit; k, id = cur.Next() {
					deComKey, _ = bPrefix.SplitKeys(k)
					_, iKey, err = codec.DecodeBytes(deComKey, iKey)
					if err != nil {
						return public.DetailError([]error{public.ErrDeCodeKey, err}, []string{string(tab)})
					}

					if bytes.HasPrefix(iKey, compKey) {
						count++
						if skip > 1 {
							if count == skip {
								count = 1
							} else {
								continue
							}
						}

						val = tx.Bucket(bPrefix.TableKey(tab)).Get(id)
						if len(val) != 0 {
							err = doParser(id, iKey, val, metaCur, tabItems)
							if err != nil {
								return err
							}
						}
					}
				}
			}
		} else {
			return public.DetailError([]error{public.ErrIdxNotFound, err}, []string{string(tab)})
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	return tabItems, nil
}

/*
Has 根据唯一索引判断key是否存在
tab 表名
index  索引名(可唯一或者非唯一索引)
key  建立索引的keys值切片

返回
uint64 不等于0则非空记录,返回的uint64类型就是记录大小
err 失败返回错误信息
**/
func (kv *KVStore) Has(tab []byte, index string, keys [][]byte) (uint64, error) {
	var (
		err   error
		count uint64

		metaCur    *public.MetaCache
		ok         bool
		isUniIndex bool
	)

	if metaCur, ok = kv.cache[string(tab)]; !ok {
		return 0, public.DetailError([]error{public.ErrTabNotFound}, []string{string(tab)})
	}
	//判断索引类型
	if metaCur.UniInds != nil {
		for i := 0; i < len(metaCur.UniInds); i++ {
			if metaCur.UniInds[i] == index {
				isUniIndex = true
				break
			}
		}
	}

	err = kv.dbIns.View(func(tx *bolt.Tx) error {
		bkt := tx.Bucket(bPrefix.IndexKey(tab, []byte(index)))
		if bkt != nil {
			var (
				k []byte
			)

			cur := bkt.Cursor()
			compKey := bPrefix.CreateCompositeKey(keys)

			if isUniIndex {
				for k, _ = cur.Seek(compKey); k != nil; k, _ = cur.Next() {
					if bytes.HasPrefix(k, compKey) {
						count++
					}
				}
			} else {
				var (
					deComKey []byte
					iKey     = make([]byte, 60)
				)

				for k, _ = cur.Seek(compKey); k != nil; k, _ = cur.Next() {
					deComKey, _ = bPrefix.SplitKeys(k)
					_, iKey, err = codec.DecodeBytes(deComKey, iKey)
					if err != nil {
						return public.DetailError([]error{public.ErrDeCodeKey, err}, []string{string(tab)})
					}

					if bytes.HasPrefix(iKey, compKey) {
						count++
					}
				}
			}
		} else {
			return public.DetailError([]error{public.ErrIdxNotFound, err}, []string{string(tab)})
		}
		return nil
	})
	if err != nil {
		return 0, err
	}

	return count, nil
}

/*
Delete 根据索引删除key-val
tab 表名
index  索引名(可唯一或者非唯一索引)
key  建立索引的keys值切片

返回
uint64 删除的记录数
err 失败返回错误信息
**/
func (kv *KVStore) Delete(tab []byte, index string, keys [][]byte) (uint64, error) {
	var (
		err error
		val []byte

		count      uint64
		metaCur    *public.MetaCache
		ok         bool
		isUniIndex bool
	)

	if metaCur, ok = kv.cache[string(tab)]; !ok {
		return 0, public.DetailError([]error{public.ErrTabNotFound}, []string{string(tab)})
	}
	//判断索引类型
	if metaCur.UniInds != nil {
		for i := 0; i < len(metaCur.UniInds); i++ {
			if metaCur.UniInds[i] == index {
				isUniIndex = true
				break
			}
		}
	}

	doDel := func(tab []byte, id []byte, val []byte, meta *public.MetaCache, tx *bolt.Tx) error {
		var (
			err error
		)

		if len(val) != 0 {
			//2 判断是否解压
			if meta.IsCompress {
				val, err = snappy.Decode(nil, val)
				if err != nil {
					return public.DetailError([]error{public.ErrDeCompress, err}, []string{string(tab)})
				}
			}

			var value model.Value
			err = proto.Unmarshal(val, &value)
			if err != nil {
				return public.DetailError([]error{public.ErrUnMarshal, err}, []string{string(tab)})
			}

			//3 删除表数据
			err = tx.Bucket(bPrefix.TableKey(tab)).Delete(id)
			if err != nil {
				return public.DetailError([]error{public.ErrDelKey, err}, []string{string(tab)})
			}

			//4 删除唯一索引数据
			if value.UniInds != nil {
				for indexName, uniKeys := range value.UniInds {
					bkt := tx.Bucket(bPrefix.IndexKey(tab, []byte(indexName)))
					if bkt != nil {
						err = bkt.Delete(bPrefix.CreateCompositeKey(uniKeys.Indkeys))
						if err != nil {
							return public.DetailError([]error{public.ErrDelKey, err}, []string{string(tab)})
						}
					}
				}
			}

			//5 删除非唯一索引数据
			if value.Inds != nil {
				for indexName, keys := range value.Inds {
					bkt := tx.Bucket(bPrefix.IndexKey(tab, []byte(indexName)))
					if bkt != nil {
						err = bkt.Delete(bPrefix.JoinKeys(codec.EncodeBytes(nil, bPrefix.CreateCompositeKey(keys.Indkeys)), id))
						if err != nil {
							return public.DetailError([]error{public.ErrDelKey, err}, []string{string(tab)})
						}
					}
				}
			}
		}
		return nil
	}

	err = kv.dbIns.Update(func(tx *bolt.Tx) error {
		bkt := tx.Bucket(bPrefix.IndexKey(tab, []byte(index)))
		if bkt != nil {
			var (
				k  []byte
				id []byte
			)

			cur := bkt.Cursor()
			compKey := bPrefix.CreateCompositeKey(keys)

			if isUniIndex {
				for k, id = cur.Seek(compKey); k != nil; k, id = cur.Next() {
					if bytes.HasPrefix(k, compKey) {
						//1 根据id索引获取值
						val = tx.Bucket(bPrefix.TableKey(tab)).Get(id)
						err = doDel(tab, id, val, metaCur, tx)
						if err != nil {
							return err
						}
						count++
					}

				}
			} else {
				var (
					deComKey []byte
					iKey     = make([]byte, 60)
				)

				for k, id = cur.Seek(compKey); k != nil; k, id = cur.Next() {
					deComKey, _ = bPrefix.SplitKeys(k)
					_, iKey, err = codec.DecodeBytes(deComKey, iKey)
					if err != nil {
						return public.DetailError([]error{public.ErrDeCodeKey, err}, []string{string(tab)})
					}

					if bytes.HasPrefix(iKey, compKey) {
						//1 根据id索引获取值
						val = tx.Bucket(bPrefix.TableKey(tab)).Get(id)
						err = doDel(tab, id, val, metaCur, tx)
						if err != nil {
							return err
						}
						count++
					}
				}
			}
		} else {
			return public.DetailError([]error{public.ErrIdxNotFound, err}, []string{string(tab)})
		}

		return nil
	})

	if err != nil {
		return 0, err
	}

	return count, nil
}

/*
NewIterator 迭代器 上层可使用迭代器实现前缀匹配、范围查询、过滤、逆序结果集
tab 表名
index 索引名
prefix 匹配前缀

返回
迭代器对象
err 失败返回错误信息
*/
func (kv *KVStore) NewIterator(tab []byte, index string, prefix [][]byte) (iterator.IIterator, error) {
	var (
		err     error
		tabName []byte
		tx      *bolt.Tx
		preKeys []byte
		iter    *bolt.Cursor

		val []byte

		iID  uint64
		ikey [][]byte
		ival []byte

		metaCur    *public.MetaCache
		ok         bool
		isCompress bool
		isUniIndex bool
	)

	if metaCur, ok = kv.cache[string(tab)]; !ok {
		return nil, public.DetailError([]error{public.ErrTabNotFound}, []string{string(tab)})
	}
	//判断索引类型
	if metaCur.UniInds != nil {
		for i := 0; i < len(metaCur.UniInds); i++ {
			if metaCur.UniInds[i] == index {
				isUniIndex = true
				break
			}
		}
	}

	isCompress = metaCur.IsCompress
	decKeys := make([]byte, 300)

	if tab != nil {
		tabName = make([]byte, len(tab))
		copy(tabName, tab)
	}

	getVals := func(keys []byte, val []byte, meta *public.MetaCache) (uint64, [][]byte, []byte, error) {
		var (
			err error
		)

		if len(val) != 0 {
			if meta.IsCompress {
				val, err = snappy.Decode(nil, val)
				if err != nil {
					return 0, nil, nil, public.DetailError([]error{public.ErrDeCompress, err}, []string{string(tab)})
				}
			}

			var value model.Value
			err = proto.Unmarshal(val, &value)
			if err != nil {
				return 0, nil, nil, public.DetailError([]error{public.ErrUnMarshal, err}, []string{string(tab)})
			}

			return codec.BToU64(value.ID), bPrefix.SplitCompositeKey(keys), value.Val, nil
		}
		return 0, nil, nil, nil
	}

	tx, err = kv.dbIns.Begin(true)
	if err != nil {
		return nil, public.DetailError([]error{public.ErrBeginTransaction, err}, []string{string(tab)})
	}

	bkt := tx.Bucket(bPrefix.IndexKey(tabName, []byte(index)))
	if bkt != nil {
		iter = bkt.Cursor() //初始游标
		if prefix != nil {
			preKeys = bPrefix.CreateCompositeKey(prefix)
			tKey, tVal := iter.Seek(preKeys)
			if tKey != nil {
				if isUniIndex {
					if bytes.HasPrefix(tKey, preKeys) {
						val = tx.Bucket(bPrefix.TableKey(tab)).Get(tVal)
						iID, ikey, ival, err = getVals(tKey, val, metaCur)
						if err != nil {
							return nil, err
						}
					}
				} else {
					deComKey, _ := bPrefix.SplitKeys(tKey)
					_, decKeys, err = codec.DecodeBytes(deComKey, decKeys)
					if err != nil {
						return nil, err
					}

					if bytes.HasPrefix(decKeys, preKeys) {
						val = tx.Bucket(bPrefix.TableKey(tabName)).Get(tVal)
						iID, ikey, ival, err = getVals(decKeys, val, metaCur)
						if err != nil {
							return nil, err
						}
					}
				}
			}
		}
	} else {
		return nil, public.DetailError([]error{public.ErrIdxNotFound, err}, []string{string(tab)})
	}

	it := iterator.NewIterator(kv.dbIns,
		kv.cache,
		tabName,
		isUniIndex,
		isCompress,
		tx,
		preKeys,
		decKeys[:0],
		iter,
		iID, ikey, ival)
	return it, nil
}
