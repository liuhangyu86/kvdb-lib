package kv

import (
	"code.uni-ledger.com/switch/kvdb-lib/database/iterator"
)

type IndexKeys struct {
	Index string
	Keys  [][]byte
}

type IDatabase interface {
	Close() error
	GetTabs(isPrint bool) (interface{}, error)

	CreateTab(tab []byte, isCompress bool, uniInds []string, inds []string) error
	DropTab(tab []byte) error

	Put(tab []byte, uniInds map[string][][]byte, inds map[string][][]byte, val []byte) (uint64, error)
	Update(tab []byte, cond *IndexKeys, uniInds []*IndexKeys, inds []*IndexKeys, val []byte) (uint64, error)

	ScanTab(tab []byte, skip uint64, limit uint64, isPrint bool) (interface{}, error)
	Get(tab []byte, uniInd string, unikeys [][][]byte) (interface{}, error)
	GetRes(tab []byte, index string, keys [][]byte, skip uint64, limit uint64) (interface{}, error)

	Has(tab []byte, index string, keys [][]byte) (uint64, error)
	Delete(tab []byte, index string, keys [][]byte) (uint64, error)

	NewBatch(tab []byte) IBatch

	NewIterator(tab []byte, index string, prefix [][]byte) (iterator.IIterator, error)
}

type IBatch interface {
	Begin() error
	Commit() error
	Rollback() error
	BatchPut(uniInds map[string][][]byte, inds map[string][][]byte, val []byte) (uint64, error)
	BatchDelete(idxType bool, index string, keys [][]byte) (uint64, error)
}
