package prefix

import "bytes"

type DataEntryPrefix byte

//ASCII不可见字符(控制字符)0~32\127
const (
	DBNAME                      = "db"
	DATA_DB     DataEntryPrefix = 0x00
	DATA_TABLE  DataEntryPrefix = 0x01
	DATA_INDEX  DataEntryPrefix = 0x02
	DATA_COMKEY DataEntryPrefix = 0x03
	PAD_FLAG0   DataEntryPrefix = 0x04
	PAD_FLAG1   DataEntryPrefix = 0x05
	PAD_FLAG2   DataEntryPrefix = 0x06
)

/*
元数据：
DB Bucket name: 0x00#db
----------------------------------------
tabname          |  Metadata
0x01-"tabName1"  | {tabName1, inds []string, time}
0x01-"tabName2"  | {tabName2, inds []string, time}
0x01-"tabName3"  | {tabName3, inds []string, time}
----------------------------------------

data表：
data Bucket name: 0x01-"tabName1"
----------------------------------------
key                 | Value
uint64_to_bytes_id1 | {ind_map{ind1:[s1,ss1,...],ind2:[sss1]},val}
uint64_to_bytes_id2 | {ind_map{ind1:[s2,ss2,...],ind2:[sss2]},val}
uint64_to_bytes_id2 | {ind_map{ind1:[s3,ss3,...],ind2:[sss3]},val}
----------------------------------------

ind1表：
index Bucket name: 0x02-"tabName1""ind1"
----------------------------------------
key                     | value
0x03+"ind1""s1""ss1"... | uint64_to_bytes_id1
0x03+"ind1""s2""ss2"... | uint64_to_bytes_id2
0x03+"ind1""s3""ss3"... | uint64_to_bytes_id3
----------------------------------------

ind2表：
index Bucket name: 0x02-"tabName1""ind2"
----------------------------------------
key                  | value
0x03+"ind2""sss1"... | uint64_to_bytes_id1
0x03+"ind2""sss2"... | uint64_to_bytes_id2
0x03+"ind2""sss3"... | uint64_to_bytes_id3
----------------------------------------


**/

func paddingKey(prefix DataEntryPrefix, pad DataEntryPrefix, keys ...[]byte) []byte {
	bytesBuf := new(bytes.Buffer)
	bytesBuf.WriteByte(byte(prefix))
	for i := range keys {
		bytesBuf.Write(keys[i])
		if i != len(keys)-1 {
			bytesBuf.WriteByte(byte(pad))
		}
	}
	return bytesBuf.Bytes()
}

func DBKey(key string) []byte {
	return paddingKey(DATA_DB, PAD_FLAG0, []byte(key))
}

func TableKey(key []byte) []byte {
	return paddingKey(DATA_TABLE, PAD_FLAG1, key)
}

func IndexKey(key ...[]byte) []byte {
	return paddingKey(DATA_INDEX, PAD_FLAG1, key...)
}

func CreateCompositeKey(keys [][]byte) []byte {
	return paddingKey(DATA_COMKEY, PAD_FLAG2, keys...)
}

func SplitCompositeKey(keys []byte) [][]byte {
	if len(keys) != 0 && keys[0] == byte(DATA_COMKEY) {
		var (
			retKeys [][]byte
			key     = make([]byte, len(keys))
		)

		for i := 1; i < len(keys); i++ {
			if keys[i] != byte(PAD_FLAG2) {
				key[i] = keys[i]
			} else {
				retKeys = append(retKeys, key)
				key = make([]byte, len(keys))
			}
		}
		retKeys = append(retKeys, key)
		return retKeys
	}
	return nil
}

func JoinKeys(key []byte, id []byte) []byte {
	return bytes.Join([][]byte{key, id}, nil)
}

func SplitKeys(key []byte) ([]byte, []byte) {
	keyLen := len(key)
	if keyLen >= 8 {
		return key[0 : keyLen-8], key[keyLen-8:]
	}
	return key, nil
}
