package codec

import (
	"math"
)

func encodeFloatToCmpUint64(f float64) uint64 {
	u := math.Float64bits(f)
	if f >= 0 {
		u |= signMask
	} else {
		u = ^u
	}
	return u
}

func decodeCmpUintToFloat(u uint64) float64 {
	if u&signMask > 0 {
		u &= ^signMask
	} else {
		u = ^u
	}
	return math.Float64frombits(u)
}

func EncodeFloat(b []byte, v float64) []byte {
	u := encodeFloatToCmpUint64(v)
	return EncodeUint(b, u)
}

func DecodeFloat(b []byte) ([]byte, float64, error) {
	b, u, err := DecodeUint(b)
	return b, decodeCmpUintToFloat(u), err
}
