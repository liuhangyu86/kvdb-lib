package codec

import "encoding/binary"

func U64ToB(v uint64) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, v)
	return b
}

func BToU64(id []byte) uint64 {
	return binary.BigEndian.Uint64(id)
}
